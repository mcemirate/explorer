package com.emirate.explorer;

public class FileComponent implements Component {
    private String name;
    private long size;

    public FileComponent(String name, long size) {
        this.name = name;
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public long getSize() {
        return size;
    }

    @Override
    public void operation() {
        System.out.println("found file");
    }
}
