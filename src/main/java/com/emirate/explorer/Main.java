package com.emirate.explorer;

public class Main {
    public static void main(String[] args) {
        FolderComposite root = new FolderComposite("root");

        FileComponent file1 = new FileComponent("", 1234);
        FileComponent file2 = new FileComponent("", 1234234234);
        FileComponent file3 = new FileComponent("", 12234);
        FileComponent file4 = new FileComponent("", 1465664);
        FolderComposite documentsFolder = new FolderComposite("documents");

        root.add(file1);
        root.add(file2);
        root.add(file3);
        root.add(file4);
        root.add(documentsFolder);

        FolderComposite importentFolder1 = new FolderComposite("importent 1");
        FolderComposite importentFolder2 = new FolderComposite("importent 2");
        FileComponent docFile1 = new FileComponent("", 1234);
        FileComponent docFile2 = new FileComponent("", 1234234234);
        FileComponent docFile3 = new FileComponent("", 1234);

        documentsFolder.add(importentFolder1);
        documentsFolder.add(importentFolder2);
        documentsFolder.add(docFile1);
        documentsFolder.add(docFile2);
        documentsFolder.add(docFile3);

        FileComponent importentFile1 = new FileComponent("", 1234234234);
        FileComponent importentFile2 = new FileComponent("", 1234);
        FileComponent importentFile3 = new FileComponent("", 1234234234);
        FileComponent importentFile4 = new FileComponent("", 1234234234);

        importentFolder1.add(importentFile1);
        importentFolder1.add(importentFile2);

        importentFolder2.add(importentFile3);
        importentFolder2.add(importentFile4);

        System.out.println(root.getSize());
        System.out.println(root.getChildrenSize());
    }
}
