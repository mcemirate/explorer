package com.emirate.explorer;

public interface Component {
    void operation();
    String getName();
    long getSize();
}
