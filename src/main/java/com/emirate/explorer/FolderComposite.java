package com.emirate.explorer;

import java.util.ArrayList;

public class FolderComposite implements Component {
    public String name;
    ArrayList<Component> children;

    public FolderComposite(String name) {
        this.name = name;
        children = new ArrayList<>();
    }

    @Override
    public void operation() {
        System.out.println("iterating through");
    }

    @Override
    public String getName() {
        return name;
    }

    public long getSize() {
        long size = 0;
        for(Component child : children) {
            size += child.getSize();
        }
        return size;
    }

    public void add(Component children) {
        this.children.add(children);
    }

    public void remove(int position) throws IndexOutOfBoundsException{
        this.children.remove(position);
    }

    public Component getChild(int position) throws IndexOutOfBoundsException{
        return this.children.get(position);
    }

    public int getChildrenSize() {
        return this.children.size();
    }
}
